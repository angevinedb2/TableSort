class SortableTable {

    constructor(tableElement) {
        this.table = tableElement;
        this.tableHead = this.table.children[0];
        this.tableBody = this.table.children[1];
        this.tableHeadRow = this.tableHead.children[0];
        this.tableHeadCells = this.tableHeadRow.children;
        this.tableBodyRows = this.tableBody.rows;
        this.order = true;
        this.decorateTable();
    }

    /*
     * Switches SortTable.order to false or vice versa.
     * 
     * @param {type} sortOrder
     * @return {undefined}
     */
    toggleSortOrder(sortOrder) {
        if (sortOrder) {
            this.order = false;
        } else {
            this.order = true;
        }
    }

    toggleSortArrow(element, sortOrder) {
        var arrow = sortOrder ? "arrow-down" : "arrow-up";
        console.log(arrow);
        element.setAttribute("class", arrow);
        console.log(element);
    }

    toggleAllSortArrows(sortOrder) {
        for (var i = 0, len = this.tableHeadCells.length; i < len; i++) {
            // Toggles the table sort span. //
            this.toggleSortArrow(this.tableHeadCells[i].children[0].children[0], sortOrder);
        }
    }

    /*
     * Determines if element a is larger than element b.
     * 
     * @param {type} a
     * @param {type} b
     * @return {Boolean}
     */
    ascendingComparator(a, b) {
        return a > b ? true : false;
    }

    /*
     * Determines if element a is smaller than element b.
     * 
     * @param {type} a
     * @param {type} b
     * @return {Boolean}
     */
    descendingComparator(a, b) {
        return a < b ? true : false;
    }

    /*
     * Compares two elements in a manner dependent on the value of sortOrder,
     * which is expected to be a boolean value.
     * 
     * @param {type} a
     * @param {type} b
     * @param {type} sortOrder
     * @return {Boolean}
     */
    comparator(a, b, sortOrder) {
        if (sortOrder) {
            return this.ascendingComparator(a, b);
        } else {
            return this.descendingComparator(a, b);
        }
    }

    /*
     * Using regular expressions, test a string to determine if it can be
     * converted into type Number.
     * 
     * @param {type} string
     * @return {Boolean}
     */
    isValidNumber(string) {
        if (string.match(/^-{0,1}\d+$/)) {
            return true;
        } else if (string.match(/^\d+\.\d+$/)) {
            return false;
        } else {
            return false;
        }
    }

    /**
     * Using regular expressions, test a string to determine if it can be
     * converted into Number.
     * 
     * @param {type} string
     * @return {Boolean}
     */
    isValidFloat(string) {
        if (string.match(/^-{0,1}\d+$/)) {
            return false;
        } else if (string.match(/^\d+\.\d+$/)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Sorting function very loosely based on W3Schools' example.
     * 
     * @param {type} column
     * @return {undefined}
     */
    sort(column) {

        var x,
                y,
                datatype,
                shouldSwitch,
                switching = true;

        while (switching) {

            switching = false;

            for (var i = 0, len = this.tableBodyRows.length; i < len - 1; i++) {
                shouldSwitch = false;
                x = this.tableBodyRows[i].getElementsByTagName("TD")[column];

                y = this.tableBodyRows[i + 1].getElementsByTagName("TD")[column];

                if (this.isValidNumber(y.innerHTML) || this.isValidFloat(y.innerHTML)) {
                    datatype = "Number";
                } else {
                    datatype = "String";
                }

                /*
                 * Switch statement designed to execute different code depending
                 * a given datatype. A very important assumption is made:
                 * it is assumed that date types are ISO 8061 compliant. Since
                 * this format as one sorts plain strings, it is unneccessary
                 * to write any code to accomdate data sorting.
                 */
                switch (datatype) {
                    case "Number":
                        shouldSwitch = this.comparator(Number(x.innerHTML), Number(y.innerHTML), this.order);
                        break;
                    case "String" :
                        shouldSwitch = this.comparator(x.innerHTML, y.innerHTML, this.order);
                        break;
                }

                /*
                 * Switches the two rows.
                 */
                if (shouldSwitch) {
                    this.tableBody.insertBefore(this.tableBodyRows[i + 1], this.tableBodyRows[i]);
                    switching = true;
                }
            }
        }

        /* 
         * Toggles the sort order after sorting has completed.
         */
        this.toggleSortOrder(this.order);
    }

    /*
     * Targets each <th> element of the current table. Appends the
     * clickableSortElement as a child of a <div> element before also
     * appending that <div> to the <th> element on the table. 
     */
    decorateTable() {

        /*
         * Stores a reference to the current object instance to call the sort
         * method on.
         */
        var instance = this;
        for (let i = 0, len = this.tableHeadCells.length; i < len; i++) {
            var clickableSortElement = document.createElement("BUTTON"),
                    clickableElementTextNode = document.createTextNode("Sort"),
                    sortElementContainer = document.createElement("SPAN");
            clickableSortElement.onclick = function () {
                instance.sort(i);
            };

            clickableSortElement.appendChild(clickableElementTextNode);
            sortElementContainer.setAttribute("class", "table-sorter-container");
            sortElementContainer.appendChild(clickableSortElement);

            this.tableHeadCells[i].appendChild(sortElementContainer);
        }
    }
}


window.onload = function () {
    var tables = document.getElementsByClassName("sortable");
    var sortableTables = [];

    for (var i = 0, len = tables.length; i < len; i++) {
        sortableTables.push(new SortableTable(tables[i]));
    }
};
